import numpy as np
import tensorflow as tf
from nltk.tokenize import word_tokenize
from gensim.models import KeyedVectors
from nltk.tokenize import word_tokenize

from pprint import pprint
import json, logging

from flask import Flask, request
from flask_cors import CORS
from flask import jsonify

from Models import model_rnet
from search import article_search_cloud


application = Flask(__name__)
cors = CORS(application)


@application.route("/", methods=['GET'])
def test():
    return "For Maraton-LB Health Check"


@application.route("/api/predict", methods=['POST'])
def predict():
    logging.basicConfig(filename='logs/prediction.log', level=logging.INFO,
                        format='%(asctime)s %(message)s')
    request_string = request.data.decode('utf-8')
    logging.info("request data %s", request_string)
    data = json.loads(request_string)
    question = data.get('question')
    article = data.get('article')
    # question = request.data.get('question')
    # article = request.data.get('article')

    logging.info("Question: %s", question)
    logging.info("Article: %s", article)

    if question is None:
        logging.info("No Question")
        return jsonify({"errors": "No data"})
    logging.info("article type %s", type(article))
    if article is None:
        logging.info('No Article')
        articles = []
        summaries = []
        resp = article_search_cloud(question)
        near_hits = resp.get('near_hits')
        if not near_hits.get('error'):
            near_hits = resp.get('near_hits').get('hits').get('hit')
            for i in range(len(near_hits)):
                articles.append(near_hits[i]['fields']['raw_text'])
                logging.info("ARTICLES %s", articles[i])
        else:
            logging.info("Error with near hits: %s", near_hits.get('error'))
        term_hits = resp.get('term_hits')
        if not term_hits.get('error'):
            term_hits = resp.get('term_hits').get('hits').get('hit')
            for i in range(len(term_hits)):
                articles.append(term_hits[i]['fields']['raw_text'])
        else:
            logging.info("Error with term search: %s", term_hits.__repr__())

        article = articles[0]
        print("Article Found!")
        # for art in articles:
        #     data = summary(art)
        #     summaries.append(data)
        #     logging.info("SUMMARY MADE: %s", data)

    logging.info("TOKENZING")
    a_token = word_tokenize(article)
    q_token = word_tokenize(question)
    paragraph = np.zeros((60, 300, 300))
    question = np.zeros((60, 30, 300))
    empty_answer_idx = np.ndarray((60, 300))

    for j in range(len(a_token)):
        if j >= 300:
            break

        try:
            paragraph[0][j][:300] = w2v_300[a_token[j]]
        except KeyError as e:
            logging.info("error finding key %s", e)

            pass
        try:
            paragraph[0][j][:300] = w2v_300[a_token[j].capitalize()]
        except KeyError as e:
            logging.info("error finding key %s", e)

            pass
        try:
            paragraph[0][j][:300] = w2v_300[a_token[j].lower()]
        except KeyError as e:
            logging.info("error finding key %s", e)

            pass
        try:
            paragraph[0][j][:300] = w2v_300[a_token[j].upper()]
        except KeyError as e:
            logging.info("error finding key %s", e)

            pass

    for j in range(len(q_token)):
        if j >= 30:
            break
        try:
            question[0][j][:300] = w2v_300[q_token[j]]
        except KeyError as e:
            logging.info("error finding key %s", e)
            pass
        try:
            question[0][j][:300] = w2v_300[q_token[j].capitalize()]
        except KeyError as e:
            logging.info("error finding key %s", e)
            pass
        try:
            question[0][j][:300] = w2v_300[q_token[j].lower()]
        except KeyError as e:
            logging.info("error finding key %s", e)
            pass
        try:
            question[0][j][:300] = w2v_300[q_token[j].upper()]
        except KeyError as e:
            logging.info("error finding key %s", e)
            pass
    logging.info("PREDICTING")

    graph = tf.get_default_graph()
    with graph.as_default():  # predict here
        predictions_si, predictions_ei = sess.run([pred_si, pred_ei], feed_dict={
            input_tensors['p']: paragraph,
            input_tensors['q']: question,
            input_tensors['a_si']: empty_answer_idx,
            input_tensors['a_ei']: empty_answer_idx,
        })

        logging.info("Finding Excerpt sentences")
        parag = a_token
        temp_sentences = []
        start_idx = int(predictions_si[0])
        end_idx = int(predictions_ei[0])
        idx = int(predictions_si[0])
        count = 0
        logging.info("finding first sentence")
        while count < 2:
            idx -= 1
            try:
                if parag[idx] in {".", "?", "!"}:
                    # temp_sentences.insert(0, ' '.join(parag[idx:start_idx]))
                    count += 1
                    print(parag[idx])
            except IndexError:
                break
        temp_sentences.insert(0, ' '.join(parag[idx:start_idx]))
        logging.info("Finding last sentence")
        idx = start_idx
        count = 0
        while count < 2:
            idx += 1
            try:
                if parag[idx] in {".", "?", "!"}:
                    # temp_sentences.append(' '.join(parag[start_idx:idx + 1]))
                    count += 1
                    print(parag[idx])
            except IndexError:
                break
        temp_sentences.append(' '.join(parag[start_idx:idx + 1]))

        # print(' '.join(pred_tokens))
        # return ' '.join(pred_tokens)

        # fact_search(question='blah', article="blah")
        f = open('test-output', 'w')
        f.write(str(article))
        f.close()
        # return ' '.join(pred_tokens)
        # temp_sentences[0], temp_sentences[1] = temp_sentences[1], temp_sentences[0]
        # temp_sentences[2], temp_sentences[3] = temp_sentences[3], temp_sentences[2]
        json_data = {'answer': ' '.join(temp_sentences)}
        logging.info("JSON DATA %s", jsonify(json_data))
        return jsonify(json_data)


if __name__ == "__main__":
    model = 'rnet'
    debug = False
    dataset = 'dev'
    model_path = 'Models/Save/rnet_model27.ckpt'
    modOpts = json.load(open('Models/config.json', 'r'))['rnet']['dev']
    pprint(modOpts)

    model = model_rnet.R_NET(modOpts)
    input_tensors, loss, acc, pred_si, pred_ei = model.build_model()
    saved_model = model_path

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    new_saver = tf.train.Saver()
    sess = tf.InteractiveSession(config=config)
    new_saver.restore(sess, saved_model)

    w2v_300 = KeyedVectors.load('Data/gensim/gensim-300.bin', mmap='r')

    print('Starting the Model API')
    application.run(port=5000, host='0.0.0.0', debug=True, use_reloader=False)


# model = 'rnet'
# debug = False
# dataset = 'dev'
# model_path = 'Models/Save/rnet_model27.ckpt'
# modOpts = json.load(open('Models/config.json', 'r'))['rnet']['dev']
# pprint(modOpts)
#
# model = model_rnet.R_NET(modOpts)
# input_tensors, loss, acc, pred_si, pred_ei = model.build_model()
# saved_model = model_path
#
# config = tf.ConfigProto()
# config.gpu_options.allow_growth = True
# new_saver = tf.train.Saver()
# sess = tf.InteractiveSession(config=config)
# new_saver.restore(sess, saved_model)
#
# w2v_300 = KeyedVectors.load('Data/gensim/gensim-300.bin', mmap='r')