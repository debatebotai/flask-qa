#!/bin/bash
python -m nltk.downloader punkt stopwords
aws s3 sync s3://debatebot-flask .
echo "done syncing"

tail -n 0 -f logs/*.log & # Monitors changes in file and outputs

# echo Starting nginx
# echo Starting Gunicorn.
#exec gunicorn wsgi:application \
 #   --name qa \
  #  --bind unix:qa.sock \
   # --workers 4 \
   # --log-level=info \
    #--log-file=logs/gunicorn.log \
   # --access-logfile=logs/access.log \
    #--preload
    #--timeout 600

#exec service nginx start

python app.py
