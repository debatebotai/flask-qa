import requests
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

import logging

logging.basicConfig(filename='logs/ArticleMatch.log', level=logging.INFO,
                    format='%(asctime)s %(message)s')

logger = logging.getLogger(__name__)
STOP_WORDS = set(stopwords.words('english'))
STOP_WORDS.add('to')
STOP_WORDS.add('the')


def parse_text(text, title=False, article=False, tokenize=False, trigrams=False):
    # If not already tokenized
    # TODO Clean text better for search
    if text and tokenize:
        text = text.lower()
        text = text.replace("'", "")
        text = text.replace('''"''', "")
        text = text.replace('\n', "")
        text = text.replace('\\', "")
        text = text.replace(r'\\', "")
        text = text.replace('\n\n', "")
        text = text.replace("(", "")
        text = text.replace(")", "")
        text = text.strip("\n")
        text_tokens = word_tokenize(text)
        for word in text_tokens:
            if word in STOP_WORDS:
                text_tokens.remove(word)
            else:
                continue
        text = text_tokens
    else:
        for word in text:
            if word in STOP_WORDS:
                text.remove(word)
            else:
                continue
    # new_title = ' '.join(title_tokens)
    # Generate Trigrams for cloudsearch fuzzy phrase matching
    trigram_tokens = []
    for i in range(0, len(text), 3):
        try:
            trigram_tokens.append((text[i: i + 3]))
        except IndexError:
            try:
                trigram_tokens.append((text[i: i + 2]))
            except IndexError:
                try:
                    trigram_tokens.append((text[i: i + 1]))
                except IndexError:
                    trigram_tokens.append((text[-1:-4]))
        # trigram_tokens.append((title_tokens[i: i+3]))
    if trigrams:
        return text, trigram_tokens
    else:
        return text


# Score based on matching phrases/terms
# TODO Return top 3 results
def article_search_cloud(text):
    logger.info('STARTING SCORING')
    title_score = {}
    text = text.strip(''' " ''')
    text_tokens, trigrams = parse_text(text, tokenize=True, trigrams=True)
    total_score = {'term': 0, 'near': 0}
    errors = {}

# Phrase Search Fuzzy. May have to limit for loop because long queries of text don't seem to work!

    # for i in trigrams[0:20]:
    #     phrase = ' '.join(i)
    #     phrase_q = phrase_q + "(near+field%s'raw_text'+distance%s3+'%s') " % ('%3D', '%3D', phrase)
    # phrase_q = phrase_q + ")&q.parser=structured&return=raw_text&size=2"
    # # base_url = \
    # #     'http://search-test-articles-yp2oisbqw4jhwawgfnpevtek4m.us-east-1.cloudsearch.amazonaws.com/2013-01-01/search?q='
    # base_url = \
    #     'http://search-asdfasdf-7tsnrzx3kxczrlxmdzw5y3tzdy.us-east-1.cloudsearch.amazonaws.com/2013-01-01/search?q='
    phrase_q = '''(or '''
    if len(trigrams) > 25:
        trigrams = trigrams[0:24]
    for i in trigrams:
        phrase = ' '.join(i)
        phrase_q = phrase_q + "(near+field%s'raw_text'+distance%s3+'%s') " % ('%3D', '%3D', phrase)
    phrase_q = phrase_q + ")&q.parser=structured&return=raw_text&size=2"
    base_url = \
        'http://search-asdfasdf-7tsnrzx3kxczrlxmdzw5y3tzdy.us-east-1.cloudsearch.amazonaws.com/2013-01-01/search?q='

    r = requests.get(base_url + phrase_q)
    logging.info('RESPONSE: %s', str(r))
    try:
        data_near = r.json()
    except Exception as e:
        logging.info('######## Error: %s', r.url)

    # Phrase Score
    logger.info("STARTING PHRASE SCORE")
    try:
        hits = data_near['hits']['hit']
        total_score['near'] = len(hits) ** 2
        logger.info('Near Score %d', total_score['near'])
    except Exception as e:
        total_score['near'] = 0
        errors['phrase'] = e.__repr__()
        data_near = {"error": e.__repr__()}

    # Term Search
    # EXAMPLE - (and+(phrase+field='title'+'star wars')+(not+(range+field%3Dyear+{,2000})))
    # Loop through each title word and append to query url in correct format for compound encoding query
    # TODO Same problem as fuzzy search. Too long of query
    term_q = '''(or '''
    for i in text_tokens[0:20]:
        term_q = term_q + "(term+field%s'raw_text'+'%s') " % ('%3D', i)
        # q = q.replace('=', '%3D')
    term_q = term_q + ')&q.parser=structured&return=raw_text&size=2'  # &return=title'
    r = requests.get(base_url + term_q)
    try:
        data_term = r.json()
    except Exception as e:
        logging.info("json request error: %s", e)
        data_term = {"error": e.__repr__()}

# Calc Term Score - each hit should match at least 50% of original words in searched title.
# If not, ignore hit. Total score is # of hits
    logger.info("STARTING TERM SCORE")
    hits = data_term['hits']['hit']
    total_score['term'] = len(hits)
    logger.info("TERM SCORE %d", total_score['term'])

    # TODO redo scoring based on how many words match. I did this for title originally, now need for raw text
    # try:
    #     hits = data_term['hits']['hit']
    #     term_score = 0
    #     hit_index = 0
    #     ratios = []
    #     for i in hits:
    #         headline = i['fields']['raw_text']  # Should already be cleaned and tokenized in database
    #         # headline = parse_text(headline)
    #         count = 0
    #         word_count = len(headline)
    #         for word in headline:
    #             if word in text_tokens:
    #                 count += 1
    #         if count != 0 and (word_count/count) <= 2:
    #             term_score += 1
    #             ratios.append((hit_index, str((float(word_count)/float(count)) * 100)))
    #             logger.info('term score added for article hit %d', hit_index)
    #         else:
    #             if count != 0:
    #                 logger.info('Not high enough count or poor ratio. Count %d and ratio %d', count, (word_count/count))
    #             logger.warning("Count of 0, can't divided by zero")
    #         hit_index += 1
    #     total_score['term'] = (term_score, ratios.__repr__())
    # except KeyError as e:
    #     print(e.__repr__())
    #     total_score['term'] = 0
    #     errors['term'] = e.__repr__()
    return {'score': total_score, 'url': r.url, 'errors': errors, 'near_hits': data_near, 'term_hits': data_term}
